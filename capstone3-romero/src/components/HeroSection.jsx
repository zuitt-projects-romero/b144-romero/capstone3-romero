import React from 'react'
import '../App.css'
import'./HeroSection.css'
import { Button } from './Button'

function HeroSection() {
    return (
        <div className='hero-container'>
            <video src='video-2.mp4' autoPlay loop muted />
        <h1>PROTECT YOUR PHONE</h1>
        <p>With <strong className='logo'>SLHD <i class="fas fa-shield-alt"></i></strong> guaranteed protection</p>
        <div className='hero-btns'>
            <Button className="btns" buttonStyle='btn--outline' buttonSize= 'btn--large'>
                VIEW CASES
            </Button>
        </div>
        </div>
    )
}

export default HeroSection
